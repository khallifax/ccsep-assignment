# A sample Makefile to use make command to build, test and run the program
# Guide: https://philpep.org/blog/a-makefile-for-your-dockerfiles/
APP=isec3004.assignment
IMG=isec3004-image

all: build

build:
	docker build -t $(IMG) .
	docker image prune -f

test:
	echo TODO: tests

stop:
	docker container stop $(APP)
	docker container rm $(APP)

run:
	docker run -i --name $(APP) $(IMG)

clean:
	docker container stop $(APP)
	docker container rm $(APP)
	docker image rm $(IMG)
	docker system prune

.PHONY: all test clean
