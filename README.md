## Git commands

https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html

# Suggested Allocation of Tasks

## Tasks

- [ ] Choose vulnerability
- [ ] Research into vulnerability
    - How to detect
    - How to exploit
    - How to patch
- [ ] Demo program
    - Vulnerable program file(s)
    - Patched program file(s)
    - Unit testing file(s)
- [ ] Presentation
- [ ] Recording

# ISEC3004 assignment



In this assignment you will make 20 minutes recorded presentation on a security bug.
You will show what the security bug is, how
to detect and finally how to mitigate it.





## Submission




1. Slides: You will make slides for your presentation.


2. Demo program: You will develop a program to support your presentation.


3. Recording: You will record your presentation in a video file. In your presentation, you talk through your slides and demo how to detect, exploit and patch the security bug.







## Recording


Your recording should include both your audio and camera feed.


Login to https://echo360.org.au/ with your student email to record
your presentation or upload your presentation video file.


Once you finish recording share your presentation,
with the unit instructor.
You will need to do the following:




1. Click to view the recording in Echo360.


2. Click on "Share" button below the video playback.


3. Under 'Individual', 'Share with:', enter the unit instructor email and press 'Done'.













OBS Studio is a good tool to record your
presentation offline.


You can find unit instructor email in BB, Contacts.











## Slides


Your slides must, at least, include the following
sections. You can include additional sections.




1. Title: Vulnerability, your names, and link to the demo program repository


2. Background: Background knowledge necessary to understand
the vulnerability. For example if your topic is DOM-based XSS
you should explain Document Object Model and
how JavaScript libraries can use it, what are Events, etc.


3. Vulnerability definition: describe the vulnerability,
describe the issue and provide a simple example with a pseudo/real code


4. Impact: Describe possible impacts of the vulnerability


5. Real-world incident: Describe a real-world incident of
the vulnerability being exploited in the wild.


6. Detection: Describe how to detect the vulnerability
through static analysis (code review) and run-time analysis


7. Prevention: Describe multiple ways to patch the vulnerability
and include pros and cons of each patch.


## Conclusion


References







## Demo program


The demo program should contain the vulnerability that your group has chosen
to demonstrate. You should write tests that cover all the functionality within
your program (these tests are later used to verify that the patched version
hasn’t lost any functionality).


You should create a patch branch in this repository.
The patch branch should contain a patched version of
your program.


Along with your demo program, you should provide a proof of concept to
demonstrate the vulnerability. This can be done in one of two ways:




1. Unit/Integration test: If you are able to write a test (unit test or integration test) that shows
the security bug and later it fails. The test fails because the program is vulnerable.
Your patched program will pass this test.


2. External tool: Write a step-by-step shows
how the vulnerability could be triggered and exploited. Include this guide
in the readme




Use this repository as a template for your demo
program. Fork, and clone this repository.
Look inside each directory for a sample file
or a guide.











Flask and JavaScript are used as examples. You are free to choose
any programming language or framework to make your program.


The patched program should pass all
your tests.


Use DockerFile and MakeFile to make your program portable and easy to
run.











## Submission checklist




 - [ ] Reviewed the assignment Rubrics in BB.


 - [ ] Demo program repository is linked in the presentation.


 - [ ] Tests cover program functionality tests.


 - [ ] A patched version of the program is created in the patch branch.


 - [ ] The patched program passes all tests.


 - [ ] Readme is updated with program description, how to run the program, how to detect, exploit and patch the bug.


 - [ ] Unnecessary template files and guides are removed.


 - [ ] Presentation is submitted to BB.


 - [ ] Presentation video recording is shared with the unit instructor.







## Contribution


In your Readme have a table that includes your name and what you have done for your assignment.

# CCSEP Assignment


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/khallifax/ccsep-assignment.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:68b1af5a4d218f7deea785bc7450b055?https://docs.gitlab.com/ee/user/application_security/sast/)

***

## Build Guide
If you ever experience this error while building, this is to do with line endings and windows.
```/bin/sh: 1: ./build-run.sh: not found```

Run this command in Git Bash to automatically fix your line endings. Or you can change each file manually in an editor of your choice.
```find . -path ./src -prune -o -type f -print0 | xargs -0 dos2unix```