# Get gcc
FROM gcc:latest

# Copy the src files
COPY . /usr/src

# Set working directory
WORKDIR /usr/src/src

# Compile and run the program
CMD ./build-run.sh
