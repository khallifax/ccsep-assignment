#include <string>
#include <iostream>
#include <stdbool.h>
#include <stdlib.h>
#include <list>
#include <iterator>
#include "vulnerable.hpp"

// This function returns a User pointer with the specified username in the "users" list
User* find(std::string username, std::list<User*>& users)
{
    std::list<User*>::iterator iter = users.begin();
    User* user = *iter;
    bool end = false;

    if(*iter == NULL)
    {
        std::cout << "\nList is empty." << std::endl;
        user = NULL;
    }
    else
    {
        do
        {
            if(user->username == username)
            {
                end = true;
            }
            else
            {
                iter++;
            }

            if(iter == users.end())
            {
                end = true;
                user = NULL;
            }
            else
            {
                user = *iter;
            }
        } while(!end);
    }

    return user;
}

// This function creates a new user, allocates it the appropriate amount of memory, and inserts it into the list
void createNewUser(bool adminSet, std::list<User*>& users)
{
    int choice = 0;
    User* newUser = (struct User*) malloc(sizeof(struct User));

    std::string username = "";
    std::string password = "";
    std::cout << "\nPlease enter new username: ";
    userInput(username);
    std::cout << "Please enter new password: ";
    userInput(password);

    newUser->username = username;
    newUser->password = password;

    if(adminSet == false)
    {
        char ch = 'n';
        std::cout << "Is this user admin? (y/n) ";
        std::cin >> ch;
        if(ch == 'y')
        {
            newUser->type = "admin";
        }
        else
        {
            newUser->type = "general";
        }
    }

    newUser->status = "active";

    users.push_back(newUser);
}

// This function allows an update of a user password with a specified username
void updateUserPassword(std::list<User*>& users)
{
    bool found = false, reset = false;
    std::string username = "";
    std::cout << "\nUsername: ";
    std::cin >> username;
    std::string password = "";

    std::list<User*>::iterator iter = users.begin();
    User* user = find(username, users);

    if(user != NULL)
    {
        while(!reset)
        {
            std::cout << "Current password: ";
            userInput(password);
            if(user->password == password)
            {
                std::cout << "New password: ";
                userInput(password);
                user->password = password;
                reset = true;
            }
        }
    }
}

// This function frees the user with a specified username in the list, but does not delete that user from the list or make it NULL
void freeUser(bool& adminSet, std::list<User*>& users)
{
    bool found = false;
    std::string username = "";
    std::cout << "\nUsername: ";
    std::cin >> username;

    std::list<User*>::iterator iter = users.begin();
    User* user = find(username, users);

    if(user != NULL)
    {
        if(user->type == "admin")
        {
            adminSet = false;
        }
        user->status = "freed";
        free(user); // free

        std::cout << "\nUser successfuly freed." << std::endl;
    }
}
 
// This function prints out all the users that have been inserted into the list
void listUsers(std::list<User*>& users)
{
    std::list<User*>::iterator iter = users.begin();
    User* user;
    bool traversed = false;
    int i = 1;

    if(*iter == NULL)
    {
        std::cout << "\nThere are no users in the database." << std::endl;
    }
    else
    {
        user = *iter;
        do
        {
            std::cout << "\nUser " << i << std::endl;
            std::cout << "    Username: " << user->username << std::endl;
            std::cout << "    Password: " << user->password << std::endl;
            std::cout << "    Type: " << user->type << std::endl;
            std::cout << "    Status: " << user->status << std::endl;
            std::cout << "    Memory address: " << user << std::endl;
            iter++;
            i++;

            if(iter == users.end())
            {
                traversed = true;
            }
            else
            {
                user = *iter;
            }
        } while(!traversed);
    } 
}

int main()
{
    bool adminSet = false;
    int choice = 0;
    std::string temp = "";
    std::list<User*> users;

    do
    {
        std::cout << "\nPlease choose from the following options:\n"
            << "    1. Create new user (with username and password)\n"
            << "    2. Edit existing user password\n"
            << "    3. Free user\n"
            << "    4. List users\n"
            << "    5. Quit" << std::endl;
        userInput(choice);

        if(choice == 1) // Create a new user
        {
            createNewUser(adminSet, users);
        }
        else if(choice == 2) // Update a password of an existing user
        {
            updateUserPassword(users);
        }
        else if(choice == 3) // Free a user (but this does not delete the user)
        {
            freeUser(adminSet, users);
        }
        else if(choice == 4) // Print out all the users recorded
        {
            listUsers(users);
        }
    } while(choice != 5);

    return 0;
}
