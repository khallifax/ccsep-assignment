#ifndef vulnerable_hpp
#define vulnerable_hpp

// This is the struct that denotes a user entity
struct User
{
    std::string username;
    std::string password;
    std::string type; // "admin" or "general"
    std::string status; // "active" or "freed"
};
    
void userInput(char& ptr)
{
    bool success = false;
    while(!success)
    {
        try
        {
            std::cin >> ptr;
            success = true;
        }
        catch(std::exception& e)
        {
            std::cout << "\nOops! Something went wrong, please try again." << std::endl;
        }
    }
}

void userInput(std::string& ptr)
{
    bool success = false;
    while(!success)
    {
        try
        {
            std::cin >> ptr;
            success = true;
        }
        catch(std::exception& e)
        {
            std::cout << "\nOops! Something went wrong, please try again." << std::endl;
        }
    }
}

void userInput(int& ptr)
{
    bool success = false;
    while(!success)
    {
        try
        {
            std::cin >> ptr;
            success = true;
        }
        catch(std::exception& e)
        {
            std::cout << "\nOops! Something went wrong, please try again." << std::endl;
        }
    }
}

#endif
